(function(angular) {
    'use strict';

    angular
        .module('Back4App')
        .directive('baTextStatus', baTextStatus)
    ;

    function baTextStatus() {
        var directive = {
            restrict: 'E',
            scope: {
                text: "@",
                sending: "=sending"
            },
            transclude: true,
            template: [
                '<span>{{ sending ? \'Loading...\' : text }}</span>'
            ].join('')
        }
        return directive;
    }
})(angular);