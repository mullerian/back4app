(function(angular) {
	'use strict';

	angular
		.module('Back4App')
		.directive('baChangeClassOnScroll', ChangeClassOnScroll)
	;

	ChangeClassOnScroll.$inject = ['$window', '$anchorScroll'];
	function ChangeClassOnScroll($window, $anchorScroll) {
		var directive = {
			link: link,
			restrict: 'A',
			scope: {
				offset: '@',
				scrollClass: '@'
			}
		}
		return directive;

		function link(scope, element, atributes) {
			$anchorScroll();
			
			angular.element($window).bind('scroll', function() {
				if(this.pageYOffset >= parseInt(scope.offset)) {
					element.addClass(scope.scrollClass);
				} else {
					element.removeClass(scope.scrollClass);
				}
			});
		}
	}
})(angular);