(function(angular) {
	'use strict';

	angular
		.module('Back4App')
		.directive('baToggleClassOnClick', baToggleClassOnClick)
	;

	function baToggleClassOnClick() {
		var directive = {
			link: link,
			restrict: 'A',
			scope: {
				target: '@',
				toggleClass: '@',
				only: '@'
			}
		}
		return directive;

		function link(scope, element, atributes) {
			element.on('click', function(event) {
				if(scope.only != undefined) {
					var onlyElement = angular.element(scope.only);
					if(onlyElement[0] != event.target) {
						return false;
					}
				}
					
				var target = angular.element(scope.target);
				target.toggleClass(scope.toggleClass);
			});
		}
	}
})(angular);