(function(angular) {
	'use strict';

	angular
		.module('Back4App', [])
		.controller('Troubleshooting', Troubleshooting)
	;

	Troubleshooting.$inject = ['dataservice'];
	function Troubleshooting(dataservice) {
		var vm = this;

        vm.commonMistakes = [];
        vm.searchResult = [];
        vm.relatedSearchs = [];
        vm.thermSearch = null;

        vm.loading = true;
        vm.activate = false;
        vm.sending = false;

        /////////////////////

        vm.search = search;

        /////////////////////

        initiate();
        function initiate() {
            var promises = [
                getCommonMistakes()
            ];

            return dataservice.ready(promises).then(function() {
                vm.activate = true;
                vm.loading = false;
            });
        }

        /////////////////////

        function getCommonMistakes() {
            return dataservice.getCommonMistakes()
                .then(function(data) {
                    if(data.success) {
                    	vm.commonMistakes = data.results;
                    }
                    return data;
                })
            ;
        }

        function search(thermSearch) {
        	vm.sending = true;
            return dataservice.search({
            	therm: thermSearch
            })
                .then(function(data) {
                	if(data.success) {
                    	vm.searchResult = data.results.search;
	                    vm.relatedSearchs = data.results.related;
                    }
                    vm.sending = false;
                    return data;
                })
            ;
        }

	}
})(angular);