(function(angular) {
    'use strict';

    angular
        .module('Back4App')
        .factory('dataservice', dataservice)
    ;

    dataservice.$inject = ['$http', '$q', '$timeout'];
    function dataservice($http, $q, $timeout) {
        var primePromise;

        var service = {
            search: search,
            getCommonMistakes: getCommonMistakes,
            ready: ready
        };
        return service;

        //////////////////////////

        function getCommonMistakes() {
        	var promise = $q.when(true);
        	// Simulate promise of protocol http
            return promise.then(function() {
            	// Simulate delay of return server
            	return $timeout(function() {
            		// Simulate data of server
            		return {
            			success: true,
            			results: [
		            		{
		            			title: 'Type somenthing',
		            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
		            		},
		            		{
		            			title: 'Type somenthing',
		            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
		            		},
		            		{
		            			title: 'Type somenthing',
		            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
		            		}
		            	]
            		};
            	}, 2000);
            });
        }

        function search(therm) {
            var promise = $q.when(true);
        	// Simulate promise of protocol http
            return promise.then(function() {
            	// Simulate delay of return server
            	return $timeout(function() {
            		// Simulate data of server
            		return {
            			success: true,
            			results: {
            				search: [
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		},
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		},
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		},
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		},
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		},
			            		{
			            			title: 'Type somenthing',
			            			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo deserunt rem magni pariatur quos perspiciatis, atque, eveniet unde laborum ipsum in cupiditate voluptates voluptas dolore ipsam cumque quam veniam.'
			            		}
			            	],
			            	related: [
			            		{
			            			title: 'Type somenthing',
			            		},
			            		{
			            			title: 'Type somenthing',
			            		},
			            		{
			            			title: 'Type somenthing',
			            		},
			            	]
            			}
            		}
            	}, 2000);
            });
        }


        ///////////////////////////

        function ready(promises) {
            if(!primePromise) {
                primePromise = $q.when(true);
            }

            return primePromise
                .then(promiseComplete)
                .catch(function() {
                    console.log('"ready" function failed in dataservice');
                })
            ;

            function promiseComplete() {
                return $q.all(promises);
            }
        }
    }
})(angular);
