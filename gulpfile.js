
var gulp   = require('gulp');
var	sass   = require('gulp-sass');

gulp.task('sass', function() {
	gulp.src('./sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch', function() {
	gulp.watch('./sass/*.scss').on('change', function() {
		gulp.start(['sass']);
	});
	gulp.watch('./sass/*/*.scss').on('change', function() {
		gulp.start(['sass']);
	});
});

gulp.task('build', function() {
	gulp.start(['sass']);
});

gulp.task('default', function() {
	gulp.start('build', 'watch');
});